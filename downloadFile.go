package main

import (
	"io"
	"net/http"
	"os"
)

func downloadFile(filepath string, url string) (err error) {
	// Create the file
	out, err := os.Create(filepath)
	checkError(err)
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	checkError(err)
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	checkError(err)

	return nil
}

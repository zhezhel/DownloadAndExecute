package main

import (
	"os"
	"os/exec"
	"strings"
)

func getBaseName() string {
	sl := strings.Split(os.Args[0], "\\")
	basename := string(sl[len(sl)-1])
	basename = basename[0 : len(basename)-4]
	return basename
}

func getDevicePath(volumeName string) (string, string, bool) {
	driveType := getDriveType()
	for _, dType := range driveType {
		out, err := exec.Command("wmic", "logicaldisk", "where",
			`VolumeName='`+volumeName+
				`' and DriveType=`+dType["sys"], "get", "deviceid").Output()
		checkError(err)
		splitedOut := strings.Split(string(out), "\n")[1:]
		for _, value := range splitedOut {
			if len(value) == 12 {
				return value[0:2], dType["api"], true
			}
		}
	}
	return "", "0", false
}

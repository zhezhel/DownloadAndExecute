src=*.go
bin=./bin

run:
	go run checkError.go downloadFile.go main.go genereteURL.go  Path_linux.go

build:
	mkdir -p $(bin)
	GOOS=windows GOARCH=386 go build -ldflags "-H windowsgui" -o $(bin)/DownloadAndExecute_windows.exe
	GOOS=linux GOARCH=386 go build -o $(bin)/DownloadAndExecute_linix
	GOOS=darwin GOARCH=386 go build -o $(bin)/DownloadAndExecute_darwin

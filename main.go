package main

import (
	"fmt"
	"os"
)

const filename string = "hello_world"
const volumeName string = "EXTMEDIA"

// https://gitlab.com/zhezhel/binary/raw/master/bin/hello_world_windows.exe

func getDriveType() map[string]map[string]string {
	driveType := make(map[string]map[string]string)
	driveType["local_usb"] = map[string]string{"api": "0", "sys": "2"}
	driveType["network_device"] = map[string]string{"api": "1", "sys": "4"}

	return driveType
}

func main() {
	basename := getBaseName()

	path, t, ok := getDevicePath(volumeName)
	if !ok {
		fmt.Println("Not found any devices!")
		return
	}
	fmt.Fprintf(os.Stdout, "path:%v", path)

	url := genereteURL(basename, t)
	_ = downloadFile(path+"/"+filename, url)

	// Pause
	// bio := bufio.NewReader(os.Stdin)
	// _, _, _ = bio.ReadLine()
}

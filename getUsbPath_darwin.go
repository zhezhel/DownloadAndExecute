package main

import (
	"fmt"
	"os/exec"
	"strings"
)

func getUsbPath() (bool, string) {
	cmd := `mount | grep "MS-DOS" | awk {'print $3'}`

	out, err := exec.Command("bash", "-c", cmd).Output()
	checkError(err)

	splitedOut := strings.Split(string(out), "\n")
	fmt.Println(splitedOut)

	if len(splitedOut) < 2 {
		return false, ""
	}

	return true, splitedOut[0]
}

package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func getBaseName() string {
	sl := strings.Split(os.Args[0], "/")
	basename := string(sl[len(sl)-1])
	return basename
}

func getDevicePath(volumeName string) (string, string, bool) {
	driveType := getDriveType()

	// Checking local storage
	cmd := `lsblk -J --output LABEL,MOUNTPOINT | grep "` + volumeName +
		`" | grep -v "null" | awk -F \" '{print  $8}' | head -n 1`
	out, err := exec.Command("bash", "-c", cmd).Output()
	checkError(err)

	outStr := strings.Trim(string(out), "\n")
	fmt.Print(outStr)
	if len(outStr) > 0 {
		return outStr, driveType["local_usb"]["api"], true
	}

	// Checking network storage
	cmd = `mount | grep ` + volumeName +
		` | awk -F on '{print $2}' | awk -F type '{print $1}' | head -n 1 |xargs`
	out, err = exec.Command("bash", "-c", cmd).Output()
	checkError(err)

	outStr = strings.Trim(string(out), "\n")
	fmt.Print(outStr)
	if len(outStr) > 0 {
		return outStr, driveType["network_device"]["api"], true
	}
	return "", "0", false
}
